import sys
import random
# import signal
from time import time
# import copy
# from operator import itemgetter

class Player_final:

    def __init__(self):
        self.available_moves = []
        # self.backup_move = (0, 0)
        self.inc_costs = [0,
         1,
         100,
         10000]
        self.INF = 1000000000
        self.initial_level = 2
        self.endtime = 6
        self.starttime = 0
        self.max_player = 1
        self.map_symbol = ['o', 'x']
        # self.blk_zob = []
        # self.blk_hash = []
        self.num_blks_won = [0, 0]
        # self.maxlen = 0
        self.mindepth = 9
        self.last_blk_won = 0
        # self.numsteps = 0
        self.dict = {}
        self.just_start = 1

    
    def move(self, board, old_move, flag):
        self.starttime = time()
        level = self.initial_level
        player = self.max_player
        # if flag == 'x':
        #     self.max_player = 1
        # else:
        #     self.max_player = 0
        self.max_player = 1 if flag == 'x' else 0
        self.timeup = 0
        self.num_blks_won = [0, 0]
        # if self.last_blk_won != 0:
        #     self.num_blks_won[self.max_player] = 1
        self.num_blks_won[self.max_player] = 1 if self.last_blk_won != 0 else self.num_blks_won[self.max_player]
        self.available_moves = board.find_valid_move_cells(old_move)
        # length = len(self.available_moves)
        prevans = self.available_moves[random.randrange(len(self.available_moves))]
        if self.just_start == 1:
            self.just_start = 0
            return prevans
        while not self.timeup:
            maxlen = max(0, len(self.dict))
            ans, val = self.move_minimax(board, old_move, player, level)
            if self.timeup:
                break
            level += 1
            prevans = ans

        status, blk_won = board.update(old_move, prevans, self.map_symbol[player])
        
        board.small_boards_status[prevans[0]][prevans[1] / 3][prevans[2] / 3] = '-'

        if blk_won == True:
            self.last_blk_won ^= 1
        else:
            self.last_blk_won = 0
        self.mindepth = min(self.mindepth, level)

        board.big_boards_status[prevans[0]][prevans[1]][prevans[2]] = '-'
        return prevans

    def move_minimax(self, board, old_move, player, level):
        self.available_moves = board.find_valid_move_cells(old_move)
        # length = len(self.available_moves)
        # best_move = self.available_moves[random.randrange(length)]
        best_move = random.choice(self.available_moves)
        temp = self.num_blks_won[player]

        if player == 0:
            opponent = 1
        else:
            opponent = 0

        maxval = -self.INF

        for move in self.available_moves:
            self.num_blks_won[player] = temp

            flag = self.map_symbol[player]

            status, blk_won = board.update(old_move, move, flag)

            # if blk_won:
            #     self.num_blks_won[player] ^= 1
            # else:
            #     self.num_blks_won[player] = 0

            self.num_blks_won[player] = self.num_blks_won[player] ^ 1 if blk_won else 0

            if blk_won and self.num_blks_won[player] == 1:

                score = self.minimax(level - 1, player, move, -self.INF, self.INF, board, player)
                self.num_blks_won[player] = 0
            else:

                score = self.minimax(level - 1, opponent, move, -self.INF, self.INF, board, player)

            board_idx = move[0]
            y_idx = move[1]
            x_idx = move[2]

            # board.big_boards_status[move[0]][move[1]][move[2]] = '-'
            # board.small_boards_status[move[0]][move[1] / 3][move[2] / 3] = '-'

            board.big_boards_status[board_idx][y_idx][x_idx] = '-'

            small_y_idx = y_idx / 3
            small_x_idx = x_idx / 3

            board.small_boards_status[board_idx][small_y_idx][small_x_idx] = '-'

            if score > maxval:
                best_move = move
                maxval = score

        self.num_blks_won[player] = temp
        return (best_move, score)

    def minimax(self, level, player, old_move, alpha, beta, board, prev_player):
        if self.timeup != 1:
            if time() - self.starttime < self.endtime:
                if not (level == 0 or board.find_terminal_state() != ('CONTINUE', '-')):
                    possible_moves = board.find_valid_move_cells(old_move)
                    score = self.INF
                    score = -self.INF if player == self.max_player else self.INF
                    temp = self.num_blks_won[player]
                    for move in possible_moves:
                        temp = self.num_blks_won[player]
                        status, blk_won = board.update(old_move, move, self.map_symbol[player])
                        temp = temp ^ 1 if blk_won else 0
                        if player == self.max_player:
                            if blk_won and temp == 1:
                                agp = self.minimax(level - 1, player, move, alpha, beta, board, player)
                                score = score if (score > agp) else agp
                                temp = 0
                            else:
                                agp = self.minimax(level - 1, player ^ 1, move, alpha, beta, board, player)
                                score = score if (score > agp) else agp
                            alpha = max(alpha, score)
                        else:
                            if blk_won and temp == 1:
                                agp = self.minimax(level - 1, player, move, alpha, beta, board, player)
                                score = score if (score < agp) else agp
                                temp = 0
                            else:
                                agp = self.minimax(level - 1, player ^ 1, move, alpha, beta, board, player)
                                score = score if (score < agp) else agp
                            beta = score if (score < beta) else beta
                        board.big_boards_status[move[0]][move[1]][move[2]] = '-'
                        # move[1] /= 3
                        # move[2] /= 3
                        board.small_boards_status[move[0]][move[1] / 3][move[2] / 3] = '-'
                        if alpha >= beta or self.timeup == 1:
                            break

                    self.num_blks_won[player] = temp
                    return score
                else:
                    return self.heuristic(board, prev_player, old_move)
            else:
                self.timeup = 1
                return self.heuristic(board, prev_player, old_move)
        else:
            return self.heuristic(board, prev_player, old_move)

    def heuristic(self, board, player, old_move):
        cur_state = board.find_terminal_state()
        if cur_state[1] != 'DRAW' and cur_state[1] != '-':
            return self.INF if (player == self.max_player) else -self.INF
        # cost = [[[0]*3]*3]*2
        cost = [[[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]]]
        for k in range(2):
            for i in range(3):
                for j in range(3):
                    if board.small_boards_status[k][i][j] != self.map_symbol[self.max_player]:
                        if board.small_boards_status[k][i][j] != self.map_symbol[self.max_player ^ 1]:
                            x = self.computecost(board, self.max_player, k, i, j)
                            cost[k][i][j] = x
                        else:
                            cost[k][i][j] = -self.INF / 100
                    else:
                        cost[k][i][j] = self.INF / 100

        # for i in range(3):
        #     for j in range(3):
        #         if board.small_boards_status[1][i][j] != self.map_symbol[self.max_player]:
        #             if board.small_boards_status[1][i][j] != self.map_symbol[self.max_player ^ 1]:
        #                 x = self.computecost(board, self.max_player, 1, i, j)
        #                 cost[1][i][j] = x
        #             else:
        #                 cost[1][i][j] = -self.INF / 100
        #         else:
        #             cost[1][i][j] = self.INF / 100

        return self.compute_for_bigboard(board, self.max_player, cost,0) + self.compute_for_bigboard(board, self.max_player, cost,1)

    def compute_for_bigboard(self, board, player, cost, board_no):
        row = [[],[],[]]
        col = [[],[],[]]
        col_tot = [0,0,0]
        row_tot = [0,0,0]

        total = 0
        for i in range(3):
            for j in range(3):
                row[i].append(board.small_boards_status[board_no][i][j])
                row_tot[i] += cost[board_no][i][j]
                col[j].append(board.small_boards_status[board_no][i][j])
                col_tot[j] += cost[board_no][i][j]

        for i in range(3):
            cntm = []
            cntm.append(row[i].count(self.map_symbol[player]))
            cntm.append(row[i].count(self.map_symbol[player ^ 1]))
            cntemp = row[i].count('-')
            if cntm[0] == 3 - cntemp or cntm[1] == 3 - cntemp:
                total += row_tot[i]

            cntm.append(col[i].count(self.map_symbol[player]))
            cntm.append(col[i].count(self.map_symbol[player ^ 1]))
            cntemp = col[i].count('-')
            if cntm[2] == 3 - cntemp or cntm[3] == 3 - cntemp:
                total += col_tot[i]

            # if len(cntm) != 4:
            #     print(len(cntm))

        diagonal = [[],[]]
        d_t = [0,0]
        for i in range(3):
            diagonal[0].append(board.small_boards_status[board_no][i][i])
            diagonal[1].append(board.small_boards_status[board_no][i][2 - i])
        for i in range(3):
            if board_no==0:
                d_t[0] += cost[board_no][i][i]
            else:
                d_t[0] += cost[board_no][0][i]
            d_t[1] += cost[board_no][i][2-i]

        for i in range(2):
            cntm = []
            cntm.append(diagonal[i].count(self.map_symbol[player]))
            cntm.append(diagonal[i].count(self.map_symbol[player ^ 1]))
            if cntm[0] + cntemp == 3 or cntm[1] + cntemp == 3:
                total += d_t[i]
        
        if total == 0:
            for i in range(3):
                for j in range(3):
                    total += cost[board_no][i][j]

        return total

    def computecost(self, board, player, board_no, row_no, col_no):
        total = 0

        row = [[],[],[]]
        col = [[],[],[]]

        i_start = 3 * row_no
        i_end = 3 * row_no + 3

        j_start = 3 * col_no
        j_end = 3 * col_no + 3

        for i in range(i_start, i_end):
            for j in range(j_start, j_end):
                row[i % 3].append(board.big_boards_status[board_no][i][j])
                col[j % 3].append(board.big_boards_status[board_no][i][j])

        for i in range(3):
            cnt = {
                'max' : 0,
                'min' : 0,
            }

            cnt['max'] = row[i].count(self.map_symbol[player])
            cnt['min'] = row[i].count(self.map_symbol[player ^ 1])

            if cnt['max'] > 0 and cnt['min'] == 0:
                total += self.inc_costs[cnt['max']]

            elif cnt['max'] == 0 and cnt['min'] > 0:
                total -= self.inc_costs[cnt['min']]

        for i in range(3):
            cnt = {
                'max' : 0,
                'min' : 0,
            }

            cnt['max'] = col[i].count(self.map_symbol[player])
            cnt['min'] = col[i].count(self.map_symbol[player ^ 1])

            if cnt['max'] > 0 and cnt['min'] == 0:
                total += self.inc_costs[cnt['max']]
                
            elif cnt['max'] == 0 and cnt['min'] > 0:
                total -= self.inc_costs[cnt['min']]

        diagonal = [[],[]]

        for j in range(3):
            diagonal[0].append(board.big_boards_status[board_no][3 * row_no + j][3 * col_no + j])
            diagonal[1].append(board.big_boards_status[board_no][3 * row_no + j][3 * col_no + 2 - j])
        
        for i in range(2):
            cntm = []
            cntm.append(diagonal[i].count(self.map_symbol[player]))
            cntm.append(diagonal[i].count(self.map_symbol[~player]))
            if cntm[0] > 0 and cntm[1] == 0:
                total += self.inc_costs[cntm[0]]
            elif cntm[0] == 0 and cntm[1] > 0:
                total -= self.inc_costs[cntm[1]]
        return total
